package Prelim.Activities;

/**
 * A fixed-size array list implementation that implements the MyList interface.
 *
 * @param <E> The type of elements stored in the list.
 */
public class MyFixedSizeArrayList<E> implements MyList<E> {
    private static final int MAX_SIZE = 5;
    private Object[] elements;
    private int size;

    /**
     * Constructs a fixed-size array list with a maximum capacity of {@code MAX_SIZE}.
     */
    public MyFixedSizeArrayList() {
        elements = new Object[MAX_SIZE];
        size = 0;
    }

    /**
     * Get the size of the fixed-size array list.
     *
     * @return The number of elements in the list.
     */
    @Override
    public int getSize() {
        return size;
    }

    /**
     * Inserts an element into the fixed-size array list.
     *
     * @param data The data to be inserted.
     * @throws ListOverflowException If the list is full and cannot accommodate more elements.
     */
    @Override
    public void insert(E data) throws ListOverflowException {
        if (size == MAX_SIZE) {
            throw new ListOverflowException("List is full. Cannot insert more elements.");
        }
        elements[size] = data;
        size++;
    }

    /**
     * Retrieves an element from the fixed-size array list by its data.
     *
     * @param data The data to search for.
     * @return The element if found; null otherwise.
     */
    @Override
    public E getElement(E data) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(data)) {
                return (E) elements[i];
            }
        }
        return null; // Element not found
    }


    /**
     * Deletes the first occurrence of an element from the fixed-size array list.
     *
     * @param data The data to be deleted.
     * @return True if the element was found and deleted; false otherwise.
     */
    @Override
    public boolean delete(E data) {
        String deleteMusicName = ((Music) data).getMusicName().toLowerCase(); // Convert to lowercase
        for (int i = 0; i < size; i++) {
            Music m = (Music) elements[i];
            if (m.getMusicName().toLowerCase().equals(deleteMusicName)) { // Convert to lowercase
                // Shift elements to fill the gap
                for (int j = i; j < size - 1; j++) {
                    elements[j] = elements[j + 1];
                }
                elements[size - 1] = null; // Clear the last element
                size--;
                return true;
            }
        }
        return false; // Element not found
    }


    /**
     * Searches for the first occurrence of an element in the fixed-size array list.
     *
     * @param data The data to search for.
     * @return The index of the first occurrence of the data in the list, or -1 if not found.
     */
    @Override
    public int search(E data) {
        String searchMusicName = ((Music) data).getMusicName().toLowerCase(); // Convert to lowercase
        for (int i = 0; i < size; i++) {
            Music m = (Music) elements[i];
            if (m.getMusicName().toLowerCase().equals(searchMusicName)) { // Convert to lowercase
                return i; // Element found, return its index
            }
        }
        return -1; // Element not found
    }

}
