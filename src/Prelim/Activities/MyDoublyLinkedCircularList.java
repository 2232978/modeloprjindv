package Prelim.Activities;

import java.util.NoSuchElementException;

public class MyDoublyLinkedCircularList<E> implements MyList<E> {
    private DoublyLinkedNode<E> head;
    private int size;

    public MyDoublyLinkedCircularList() {
        head = null;
        size = 0;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void insert(E data) throws ListOverflowException {
        // Create a new node with the given data
        DoublyLinkedNode<E> newNode = new DoublyLinkedNode<>(data);

        if (head == null) {
            // If the list is empty, set the new node as the head and make it circular
            head = newNode;
            head.setNext(head);
            head.setPrevious(head);
        } else {
            // Insert the new node at the end of the list
            DoublyLinkedNode<E> lastNode = head.getPrevious();
            lastNode.setNext(newNode);
            newNode.setPrevious(lastNode);
            newNode.setNext(head);
            head.setPrevious(newNode);
        }

        size++;
    }

    @Override
    public E getElement(E data) throws NoSuchElementException {
        DoublyLinkedNode<E> current = head;
        for (int i = 0; i < size; i++) {
            if (current.getData().equals(data)) {
                return current.getData();
            }
            current = current.getNext();
        }
        throw new NoSuchElementException("Element not found in the list");
    }

    @Override
    public boolean delete(E data) {
        DoublyLinkedNode<E> current = head;
        for (int i = 0; i < size; i++) {
            if (current.getData().equals(data)) {
                // Found the node to delete
                DoublyLinkedNode<E> prevNode = current.getPrevious();
                DoublyLinkedNode<E> nextNode = current.getNext();
                prevNode.setNext(nextNode);
                nextNode.setPrevious(prevNode);
                size--;
                if (i == 0) {
                    // If deleting the head, update head to the next node
                    head = nextNode;
                }
                return true;
            }
            current = current.getNext();
        }
        return false; // Data not found in the list
    }

    @Override
    public int search(E data) {
        DoublyLinkedNode<E> current = head;
        for (int i = 0; i < size; i++) {
            if (current.getData().equals(data)) {
                return i;
            }
            current = current.getNext();
        }
        return -1; // Data not found in the list
    }

    public DoublyLinkedNode<E> getHead() {
        return head;
    }

}

