/**
 * NAME: Modelo Alexx Evan O.
 * CLASS CODE: 9342 CS211
 * DATE: September 11, 2023
 *
 */

package Prelim.Activities;

/**
 * A class representing a node in a doubly-linked list.
 * @param <T> The type of data stored in the node
 */
class DoublyLinkedNode<T> {
    private T data;
    private DoublyLinkedNode<T> next;
    private DoublyLinkedNode<T> previous;

    /**
     * Constructs a new doubly-linked node with the given data.
     * @param data The data to be stored in the node.
     */
    public DoublyLinkedNode(T data) {
        this.data = data;
        this.next = null;
        this.previous = null;
    }

    /**
     * Gets the data stored in the node.
     * @return The data stored in the node.
     */
    public T getData() {
        return data;
    }

    /**
     * Sets the data stored in the node.
     * @param data The data to be stored in the node.
     */
    public void setData(T data) {
        this.data = data;
    }

    /**
     * Gets the next node in the list.
     * @return The next node in the list.
     */
    public DoublyLinkedNode<T> getNext() {
        return next;
    }

    /**
     * Sets the next node in the list.
     * @param next The next node in the list.
     */
    public void setNext(DoublyLinkedNode<T> next) {
        this.next = next;
    }

    /**
     * Gets the previous node in the list.
     * @return The previous node in the list.
     */
    public DoublyLinkedNode<T> getPrevious() {
        return previous;
    }

    /**
     * Sets the previous node in the list.
     * @param previous The previous node in the list.
     */
    public void setPrevious(DoublyLinkedNode<T> previous) {
        this.previous = previous;
    }
}


