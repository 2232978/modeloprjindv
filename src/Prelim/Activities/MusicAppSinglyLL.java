/**
 * NAME: Modelo Alexx Evan O.
 * CLASS CODE: 9342 CS211
 * DATE: September 11, 2023
 *
 * This class implements a simple interactive program to demonstrate
 * the usage of a singly-linked list (MySinglyLinkedList) by allowing
 * the user to add, search, and remove elements from the list.
 */

package Prelim.Activities;

import java.util.Scanner;

public class MusicAppSinglyLL {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        MySinglyLinkedList<Music> musicList = new MySinglyLinkedList<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("This Class implements the MySinglyLinkedList class.\n");

        while (true) {
            System.out.println("Options:");
            System.out.println("1. Add a music");
            System.out.println("2. Display all music");
            System.out.println("3. Search for a music");
            System.out.println("4. Delete a music");
            System.out.println("5. Get the size of the list");
            System.out.println("6. Exit");
            System.out.print("Select an option: ");

            int choice = scanner.nextInt();
            scanner.nextLine(); // Consume the newline character

            switch (choice) {
                case 1:
                    System.out.print("Enter music name: ");
                    String musicName = scanner.nextLine();
                    System.out.print("Enter genre: ");
                    String genre = scanner.nextLine();
                    System.out.print("Enter artist: ");
                    String artist = scanner.nextLine();

                    Music music = new Music(musicName, genre, artist);
                    try {
                        musicList.insert(music);
                        System.out.println("Music added successfully.");
                    } catch (ListOverflowException e) {
                        System.out.println("List overflow exception: " + e.getMessage());
                    }
                    break;
                case 2:
                    if (musicList.isEmpty()) {
                        System.out.println("Music List is empty.");
                    } else {
                        System.out.println("Music List:");
                        musicList.display();
                    }
                    break;

                case 3:
                    System.out.print("Enter music name to search: ");
                    String searchName = scanner.nextLine().toLowerCase(); // Convert the search query to lowercase
                    int searchIndex = -1;
                    int currentIndex = 0;

                    Node<Music> currentNode = musicList.getHead(); // Assuming you have a method to get the head node

                    while (currentNode != null) {
                        Music currentMusic = currentNode.getData();
                        if (currentMusic.getMusicName().toLowerCase().equals(searchName)) {
                            searchIndex = currentIndex;
                            break; // Stop searching once a match is found
                        }
                        currentNode = currentNode.getNext();
                        currentIndex++;
                    }

                    if (searchIndex != -1) {
                        System.out.println("Music found at index " + searchIndex);
                    } else {
                        System.out.println("Music not found in the list.");
                    }
                    break;


                case 4:
                    System.out.print("Enter music name to delete: ");
                    String deleteName = scanner.nextLine();

                    boolean deleted = false;
                    Node<Music> current = musicList.getHead();
                    Node<Music> previous = null;

                    while (current != null) {
                        Music currentMusic = current.getData();
                        if (currentMusic.getMusicName().equals(deleteName)) {
                            // Found the music to delete
                            if (previous == null) {
                                // If the music to delete is the head, update the head by moving to the next node
                                musicList.setHead(current.getNext());
                            } else {
                                // If it's not the head, update the previous node's 'next' reference
                                previous.setNext(current.getNext());
                            }
                            deleted = true;
                            break; // Exit the loop after deleting the music
                        }

                        previous = current;
                        current = current.getNext();
                    }

                    if (deleted) {
                        System.out.println("Music deleted successfully.");
                    } else {
                        System.out.println("Music not found in the list.");
                    }
                    break;



                case 5:
                    int size = musicList.getSize();
                    System.out.println("Size of the list: " + size);
                    break;
                case 6:
                    System.out.println("Exiting program.");
                    scanner.close();
                    System.exit(0);
                default:
                    handleInvalidChoice();
            }
        }
    }
    private static void handleInvalidChoice() {
        System.out.println("Invalid option. Please select a valid option.");
    }
}
