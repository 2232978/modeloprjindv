package Prelim.Activities;

/**
 * A node class that represents an element in a singly-linked list.
 * @param <T> The type of data stored in the node.
 */
class Node<T> {
    private T data;
    protected Node<T> next;
    private Node<T> head;

    /**
     * Constructs a new node with the given data.
     * @param data The data to be stored in the node.
     */
    public Node(T data) {
        this.data = data;
        this.next = null;
    }

    /**
     * Gets the data stored in the node.
     *
     * @return The data stored in the node.
     */
    public T getData() {
        return data;
    }

    /**
     * Sets the data stored in the node.
     * @param data The data to be stored in the node.
     */
    public void setData(T data) {
        this.data = data;
    }

    /**
     * Gets the next node in the list.
     * @return The next node in the list.
     */
    public Node<T> getNext() {
        return next;
    }

    /**
     * Sets the next node in the list.
     * @param next The next node in the list.
     */
    public void setNext(Node<T> next) {
        this.next = next;
    }

}

