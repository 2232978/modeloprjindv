/**
 * NAME: Modelo Alexx Evan O.
 * CLASS CODE: 9342 CS211
 * DATE: September 11, 2023
 *
 */


/**
 * A simple implementation of a singly-linked list.
 * @param <T> The type of elements stored in the list.
 */
package Prelim.Activities;


import java.util.NoSuchElementException;


public class MySinglyLinkedList<T> implements MyList<T> {

    private Node<T> head;

    /**
     * Constructs an empty singly-linked list.
     */
    public MySinglyLinkedList() {
        this.head = null;
    }
    @Override
    public int getSize() {
        int size = 0;
        Node<T> current = head;
        while (current != null) {
            size++;
            current = current.getNext();
        }
        return size;
    }

    @Override
    public void insert(T data) throws ListOverflowException {
        // You can add elements to the end of the list.
        Node<T> newNode = new Node<>(data);

        // If the list is empty, set the new node as the head.
        if (head == null) {
            head = newNode;
            return;
        }

        Node<T> current = head;

        // Traverse to the end of the list.
        while (current.getNext() != null) {
            current = current.getNext();
        }

        // Set the new node as the next node of the current last node.
        current.setNext(newNode);
    }

    @Override
    public T getElement(T data) throws NoSuchElementException {
        Node<T> current = head;
        while (current != null) {
            if (current.getData().equals(data)) {
                return current.getData();
            }
            current = current.getNext();
        }
        throw new NoSuchElementException("Element not found in the list");
    }

    @Override
    public boolean delete(T data) {
        if (head == null) {
            return false;
        }
        if (head.getData().equals(data)) {
            head = head.getNext();
            return true;
        }
        Node<T> current = head;
        while (current.getNext() != null) {
            if (current.getNext().getData().equals(data)) {
                current.setNext(current.getNext().getNext());
                return true;
            }
            current = current.getNext();
        }
        return false;
    }

    @Override
    public int search(T data) {
        int index = 0;
        Node<T> current = head;
        while (current != null) {
            if (current.getData().equals(data)) {
                return index;
            }
            current = current.getNext();
            index++;
        }
        return -1;
    }


    /**
     * Adds a new element to the end of the list.
     * @param data The data to be added to the list.
     */
    public void add(T data) {
        Node<T> newNode = new Node<>(data);
        if (head == null) {
            head = newNode;
        } else {
            Node<T> current = head;
            while (current.getNext() != null) {
                current = current.getNext();
            }
            current.setNext(newNode);
        }
    }

    /**
     * Removes the first occurrence of the specified data from the list.
     * @param data The data to be removed from the list.
     */
    public void remove(T data) {
        if (head == null) {
            return;
        }
        if (head.getData().equals(data)) {
            head = head.getNext();
            return;
        }
        Node<T> current = head;
        while (current.getNext() != null) {
            if (current.getNext().getData().equals(data)) {
                current.setNext(current.getNext().getNext());
                return;
            }
            current = current.getNext();
        }
    }

    /**
     * Displays the elements of the list in order.
     */
    public void display() {
        Node<T> current = head;
        while (current != null) {
            System.out.print(current.getData() + " -> ");
            current = current.getNext();
        }
        System.out.println("null");
    }

    /**
     * Checks if the list contains the specified element.
     * @param data The element to check for in the list.
     * @return True if the element is found in the list, false otherwise.
     */
    public boolean contains(T data) {
        Node<T> current = head;
        while (current != null) {
            if (current.getData().equals(data)) {
                return true;
            }
            current = current.getNext();
        }
        return false;
    }
    public Node<T> getHead() {
        return head;
    }
    public void setHead(Node<T> newHead) {
        this.head = newHead;
    }
    public boolean isEmpty() {
        return head == null;
    }

}

