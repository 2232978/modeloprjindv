/**
 * NAME: Modelo Alexx Evan O.
 * CLASS CODE: 9342 CS211
 * DATE: September 11, 2023
 *
 */

package Prelim.Activities;

import java.util.ArrayList;
import java.util.Scanner;

public class MusicAppFixedArr {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        ArrayList<Music> musicList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("This Class implements the MyFixedArrayList class.\n");
        while (true) {

            System.out.println("Options:");
            System.out.println("1. Insert a music record");
            System.out.println("2. Search for a music record");
            System.out.println("3. Delete a music record");
            System.out.println("4. Print the music list");
            System.out.println("5. Exit");
            System.out.print("Enter your choice: ");

            int choice = scanner.nextInt();
            scanner.nextLine(); // Consume the newline character

            switch (choice) {
                case 1:
                    System.out.print("Enter music name: ");
                    String musicName = scanner.nextLine();
                    System.out.print("Enter genre: ");
                    String genre = scanner.nextLine();
                    System.out.print("Enter artist: ");
                    String artist = scanner.nextLine();

                    Music music = new Music(musicName, genre, artist);
                    musicList.add(music);
                    System.out.println("Music record inserted successfully.");
                    break;

                case 2:
                    System.out.print("Enter music name to search: ");
                    String searchMusicName = scanner.nextLine();

                    boolean found = false;
                    for (Music m : musicList) {
                        if (m.getMusicName().equalsIgnoreCase(searchMusicName)) {
                            System.out.println("Music record found:");
                            System.out.println(m);
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        System.out.println("Music record not found.");
                    }
                    break;

                case 3:
                    System.out.print("Enter music name to delete: ");
                    String deleteMusicName = scanner.nextLine();

                    boolean deleted = false;
                    for (Music m : musicList) {
                        if (m.getMusicName().equalsIgnoreCase(deleteMusicName)) {
                            musicList.remove(m);
                            System.out.println("Music record deleted.");
                            deleted = true;
                            break;
                        }
                    }

                    if (!deleted) {
                        System.out.println("Music record not found for deletion.");
                    }
                    break;

                case 4:
                    System.out.println("Music List:");
                    for (int i = 0; i < musicList.size(); i++) {
                        Music m = musicList.get(i);
                        System.out.println("Index " + i + ": " + m);
                    }
                    break;

                case 5:
                    System.out.println("Exiting the program.");
                    scanner.close();
                    System.exit(0);
                    break;

                default:
                    System.out.println("Invalid choice. Please enter a valid option.");
            }
        }
    }
}

