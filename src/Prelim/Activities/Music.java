package Prelim.Activities;

/**
 * The Music class represents a music item with attributes such as music name, genre, and artist.
 * It implements the Comparable interface to allow comparison based on musicName.
 */
public class Music implements Comparable<Music> {
    private String musicName;
    private String genre;
    private String artist;

    /**
     * Constructs a new Music object with the specified attributes.
     *
     * @param musicName The name of the music.
     * @param genre     The genre of the music.
     * @param artist    The artist of the music.
     */
    public Music(String musicName, String genre, String artist) {
        this.musicName = musicName;
        this.genre = genre;
        this.artist = artist;
    }

    /**
     * Gets the name of the music.
     *
     * @return The music name.
     */
    public String getMusicName() {
        return musicName;
    }

    /**
     * Gets the genre of the music.
     *
     * @return The music genre.
     */
    public String getGenre() {
        return genre;
    }

    /**
     * Gets the artist of the music.
     *
     * @return The music artist.
     */
    public String getArtist() {
        return artist;
    }

    /**
     * Sets the name of the music.
     *
     * @param musicName The music name to set.
     */
    public void setMusicName(String musicName) {
        this.musicName = musicName;
    }

    /**
     * Sets the genre of the music.
     *
     * @param genre The music genre to set.
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * Sets the artist of the music.
     *
     * @param artist The music artist to set.
     */
    public void setArtist(String artist) {
        this.artist = artist;
    }

    /**
     * Returns a string representation of the Music object.
     *
     * @return A string containing music details.
     */
    @Override
    public String toString() {
        return "Music Name: " + musicName + ", Genre: " + genre + ", Artist: " + artist;
    }

    /**
     * Compares this Music object to another based on their musicName.
     *
     * @param other The Music object to compare to.
     * @return A negative value if this music's name is less, zero if equal, or a positive value if greater.
     */
    @Override
    public int compareTo(Music other) {
        return this.musicName.compareTo(other.musicName);
    }

    /**
     * Compares this Music object to another for equality based on attributes.
     *
     * @param o The object to compare to.
     * @return True if the objects are equal, false otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Music otherMusic = (Music) o;

        if (!musicName.equals(otherMusic.musicName)) return false;
        if (!genre.equals(otherMusic.genre)) return false;
        return artist.equals(otherMusic.artist);
    }
}
