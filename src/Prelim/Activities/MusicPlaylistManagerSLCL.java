package Prelim.Activities;


import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * A class that implements a music playlist manager using MySinglyLinkedCircularList.
 */
public class MusicPlaylistManagerSLCL {

    /**
     * Main method to run the music playlist manager.
     *
     * @param args Command line arguments (not used).
     */
    public static void main(String[] args) {
        MySinglyLinkedCircularList<Music> playlist = new MySinglyLinkedCircularList<>();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            printMenu();
            int choice = getUserChoice(scanner);

            switch (choice) {
                case 1:
                    addMusicToPlaylist(scanner, playlist);
                    break;
                case 2:
                    removeMusicFromPlaylist(scanner, playlist);
                    break;
                case 3:
                    displayPlaylist(playlist);
                    break;
                case 4:
                    searchMusic(scanner, playlist);
                    break;
                case 5:
                    rotatePlaylist(playlist); // Call the rotate method here
                    break;
                case 6:
                    System.out.println("Exiting the Music Playlist Manager.");
                    scanner.close();
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid choice. Please select a valid option.");
                    break;
            }
        }
    }

    /**
     * Print the menu of options.
     */
    private static void printMenu() {
        System.out.println("\nMusic Playlist Manager Menu:");
        System.out.println("1. Add Music to Playlist");
        System.out.println("2. Remove Music from Playlist");
        System.out.println("3. Display Playlist");
        System.out.println("4. Search Music in Playlist");
        System.out.println("5. Rotate Playlist");
        System.out.println("6. Exit");
        System.out.print("Enter your choice: ");
    }

    /**
     * Get the user's menu choice.
     *
     * @param scanner Scanner object for user input.
     * @return The user's choice as an integer.
     */
    private static int getUserChoice(Scanner scanner) {
        try {
            return scanner.nextInt();
        } catch (InputMismatchException e) {
            scanner.nextLine(); // Consume the invalid input
            return -1;
        }
    }

    /**
     * Add music to the playlist.
     *
     * @param scanner  Scanner object for user input.
     * @param playlist The playlist to which music will be added.
     */
    private static void addMusicToPlaylist(Scanner scanner, MySinglyLinkedCircularList<Music> playlist) {
        scanner.nextLine(); // Consume the previous newline character

        System.out.print("Enter music name: ");
        String musicName = scanner.nextLine().trim(); // Read the entire line and remove leading/trailing spaces

        System.out.print("Enter genre: ");
        String genre = scanner.nextLine().trim(); // Read the genre and remove leading/trailing spaces

        System.out.print("Enter artist: ");
        String artist = scanner.nextLine().trim(); // Read the artist and remove leading/trailing spaces

        Music newMusic = new Music(musicName, genre, artist);
        playlist.insert(newMusic);
        System.out.println("Added to playlist: " + newMusic);
    }

    private static void removeMusicFromPlaylist(Scanner scanner, MySinglyLinkedCircularList<Music> playlist) {
        System.out.print("Enter music name to delete: ");
        scanner.nextLine(); // Consume any remaining newline characters

        String deleteName = scanner.nextLine();

        boolean deleted = false;
        Node<Music> current = playlist.getHead();
        Node<Music> previous = null;

        while (current != null) {
            Music currentMusic = current.getData();
            if (currentMusic.getMusicName().equals(deleteName)) {
                // Found the music to delete
                if (previous == null) {
                    // If the music to delete is the head, update the head by moving to the next node
                    playlist.setHead(current.getNext());
                } else {
                    // If it's not the head, update the previous node's 'next' reference
                    previous.setNext(current.getNext());
                }
                deleted = true;
                break; // Exit the loop after deleting the music
            }

            previous = current;
            current = current.getNext();
        }

        if (deleted) {
            System.out.println("Music deleted successfully.");
        } else {
            System.out.println("Music not found in the playlist.");
        }
    }





    /**
     * Display the playlist.
     *
     * @param playlist The playlist to display.
     */
    private static void displayPlaylist(MySinglyLinkedCircularList<Music> playlist) {
        System.out.println("\nPlaylist:");
        for (int i = 0; i < playlist.getSize(); i++) {
            try {
                System.out.println((i + 1) + ". " + playlist.getElementAt(i));
            } catch (NoSuchElementException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    /**
     * Search for music in the playlist.
     *
     * @param scanner  Scanner object for user input.
     * @param playlist The playlist to search in.
     */
    private static void searchMusic(Scanner scanner, MySinglyLinkedCircularList<Music> playlist) {
        System.out.print("Enter music name to search: ");
        scanner.nextLine(); // Consume the newline character from the previous input
        String searchName = scanner.nextLine().toLowerCase(); // Convert the search query to lowercase
        int searchIndex = -1;
        int currentIndex = 0;

        Node<Music> currentNode = playlist.getHead(); // Assuming you have a method to get the head node

        while (currentNode != null) {
            Music currentMusic = currentNode.getData();
            if (currentMusic.getMusicName().toLowerCase().equals(searchName)) {
                searchIndex = currentIndex;
                break; // Stop searching once a match is found
            }
            currentNode = currentNode.getNext();
            currentIndex++;
        }

        if (searchIndex != -1) {
            System.out.println("Music found at index " + searchIndex);
        } else {
            System.out.println("Music not found in the playlist.");
        }
    }




    /**
     * Rotate the playlist, making the next song the current playing song.
     *
     * @param playlist The playlist to rotate.
     */
    private static void rotatePlaylist(MySinglyLinkedCircularList<Music> playlist) {
        try {
            playlist.rotate();
            System.out.println("Playlist rotated. Now playing: " + playlist.getElementAt(0));
        } catch (NoSuchElementException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

}

