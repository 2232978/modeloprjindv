/**
 * NAME: Modelo Alexx Evan O.
 * CLASS CODE: 9342 CS211
 * DATE: September 11, 2023
 */

package Prelim.Activities;

import java.util.Arrays;
import java.util.NoSuchElementException;

/**
 * A dynamically growing array list implementation that implements the MyList interface.
 *
 * @param <E> The type of elements stored in the list.
 */
public class MyGrowingArrayList<E> implements MyList<E> {
    private static final int INITIAL_CAPACITY = 5;
    private Object[] elements;
    private int size;

    /**
     * Constructs a dynamically growing array list with an initial capacity of {@code INITIAL_CAPACITY}.
     */
    public MyGrowingArrayList() {
        elements = new Object[INITIAL_CAPACITY];
        size = 0;
    }

    /**
     * Get the size of the dynamically growing array list.
     *
     * @return The number of elements in the list.
     */
    @Override
    public int getSize() {
        return size;
    }

    /**
     * Inserts an element into the dynamically growing array list.
     *
     * @param data The data to be inserted.
     * @throws ListOverflowException If an internal error occurs (not applicable to this implementation).
     */
    @Override
    public void insert(E data) throws ListOverflowException {
        if (size == elements.length) {
            // If the array is full, create a new array with double the size
            int newCapacity = elements.length * 2;
            elements = Arrays.copyOf(elements, newCapacity);
            System.out.println("List doubled in size. New capacity: " + newCapacity);
        }
        elements[size] = data;
        size++;
    }


    /**
     * Retrieves the first occurrence of an element in the dynamically growing array list.
     *
     * @param data The data to search for.
     * @return The first occurrence of the data in the list.
     * @throws NoSuchElementException If the element is not found in the list.
     */
    @Override
    public E getElement(E data) throws NoSuchElementException {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(data)) {
                return (E) elements[i];
            }
        }
        throw new NoSuchElementException("Element not found in the list.");
    }

    /**
     * Deletes the first occurrence of an element from the dynamically growing array list.
     *
     * @param data The data to be deleted.
     * @return True if the element was found and deleted; false otherwise.
     */
    @Override
    public boolean delete(E data) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(data)) {
                // Shift elements to fill the gap
                for (int j = i; j < size - 1; j++) {
                    elements[j] = elements[j + 1];
                }
                elements[size - 1] = null; // Clear the last element
                size--;
                return true;
            }
        }
        return false; // Element not found
    }

    /**
     * Searches for the first occurrence of an element in the dynamically growing array list.
     *
     * @param data The data to search for.
     * @return The index of the first occurrence of the data in the list, or -1 if not found.
     */
    @Override
    public int search(E data) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(data)) {
                return i; // Element found, return its index
            }
        }
        return -1; // Element not found
    }
}
