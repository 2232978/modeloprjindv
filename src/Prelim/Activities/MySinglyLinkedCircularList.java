package Prelim.Activities;

import java.util.NoSuchElementException;

/**
 * A singly linked circular list implementation that implements the MyList interface.
 *
 * @param <T> The type of elements stored in the list.
 */
public class MySinglyLinkedCircularList<T> implements MyList<T> {
    private Node<T> head;
    private int size;

    /**
     * Constructs an empty singly linked circular list.
     */
    public MySinglyLinkedCircularList() {
        head = null;
        size = 0;
    }

    /**
     * Get the size of the singly linked circular list.
     *
     * @return The number of elements in the list.
     */
    @Override
    public int getSize() {
        return size;
    }

    /**
     * Inserts an element into the singly linked circular list.
     *
     * @param data The data to be inserted.
     * @throws ListOverflowException If an internal error occurs (not applicable to this implementation).
     */
    @Override
    public void insert(T data) throws ListOverflowException {
        Node<T> newNode = new Node<>(data);
        if (head == null) {
            // If the list is empty, make the new node the head and point it to itself
            head = newNode;
            head.setNext(head);
        } else {
            // Find the last node and make it point to the new node
            Node<T> current = head;
            while (current.getNext() != head) {
                current = current.getNext();
            }
            newNode.setNext(head); // Make the new node point to the head
            current.setNext(newNode); // Update the current tail node to point to the new node
        }
        size++;
    }

    /**
     * Retrieves the first occurrence of an element in the singly linked circular list.
     *
     * @param data The data to search for.
     * @return The first occurrence of the data in the list.
     * @throws NoSuchElementException If the element is not found in the list.
     */
    @Override
    public T getElement(T data) throws NoSuchElementException {
        Node<T> current = head;
        for (int i = 0; i < size; i++) {
            if (current.getData().equals(data)) {
                return current.getData();
            }
            current = current.getNext();
        }
        throw new NoSuchElementException("Element not found in the list");
    }

    /**
     * Deletes the first occurrence of an element from the singly linked circular list.
     *
     * @param data The data to be deleted.
     * @return True if the element was found and deleted; false otherwise.
     */
    @Override
    public boolean delete(T data) {
        if (head == null) {
            return false; // List is empty
        }

        if (head.getData().equals(data)) {
            // If the head node contains the data to be deleted
            if (size == 1) {
                head = null;
            } else {
                Node<T> current = head;
                while (current.getNext() != head) {
                    current = current.getNext();
                }
                head = head.getNext();
                current.setNext(head);
            }
            size--;
            return true;
        } else {
            Node<T> current = head;
            Node<T> prev = null;

            do {
                if (current.getData().equals(data)) {
                    prev.setNext(current.getNext());
                    size--;
                    return true;
                }
                prev = current;
                current = current.getNext();
            } while (current != head);

            return false; // Data not found in the list
        }
    }

    /**
     * Searches for an element in the singly linked circular list.
     *
     * @param data The data to search for.
     * @return The index of the first occurrence of the data in the list, or -1 if not found.
     */
    @Override
    public int search(T data) {
        Node<T> current = head;
        for (int i = 0; i < size; i++) {
            if (isEqual(current.getData(), data)) {
                return i;
            }
            current = current.getNext();
        }
        return -1; // Data not found in the list
    }
    /**
     * Helper method to check if two elements are considered equal.
     *
     * @param element1 The first element.
     * @param element2 The second element.
     * @return True if the elements are considered equal, false otherwise.
     */
    private boolean isEqual(T element1, T element2) {
        if (element1 == null && element2 == null) {
            return true; // Both elements are null, consider them equal
        }

        if (element1 == null || element2 == null) {
            return false; // One of the elements is null, consider them not equal
        }

        // Add more type-specific checks as needed
        if (element1 instanceof String && element2 instanceof String) {
            return ((String) element1).equalsIgnoreCase((String) element2);
        }

        // Default to using the equals method for other types
        return element1.equals(element2);
    }

    /**
     * Retrieves the element at a specified index in the singly linked circular list.
     *
     * @param index The index of the element to retrieve.
     * @return The element at the specified index.
     * @throws NoSuchElementException If the index is out of bounds.
     */
    public T getElementAt(int index) throws NoSuchElementException {
        if (index < 0 || index >= size) {
            throw new NoSuchElementException("Index out of bounds");
        }

        Node<T> current = head;
        for (int i = 0; i < index; i++) {
            current = current.getNext();
        }
        return current.getData();
    }

    /**
     * Rotates the elements of the singly linked circular list.
     *
     * @throws NoSuchElementException If the list is empty.
     */
    public void rotate() throws NoSuchElementException {
        if (head == null) {
            throw new NoSuchElementException("List is empty");
        }

        head = head.getNext(); // Move the head to the next node
    }

    /**
     * Get the head node of the singly linked circular list.
     *
     * @return The head node of the list.
     */
    public Node<T> getHead() {
        return head;
    }

    public void setHead(Node<T> next) {
        this.head = head;
    }

    // Hi sir, thanks for being an amazing teacher :3
}
