/**
 * NAME: Modelo Alexx Evan O.
 * CLASS CODE: 9342 CS211
 * DATE: September 18, 2023
 *
 *          DOUBLY LINKED LIST IMPLEMENTATION
 *
 *  "A PRACTICE TOO ON JOPTIONPANE GUI"
 *  This code creates a GUI application using Swing and JOptionPane dialogs to interact with the playlist.
 *  It provides buttons to add, remove, display, search, get the size of the playlist,display the list
 *  forward or backward and exit the program. The input is collected through dialog boxes, and the results
 *  are displayed using dialog boxes as well.
 */

package Prelim.Activities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MusicPlaylistManagerDLL {
    private MyDoublyLinkedList<Music> playlist;

    public MusicPlaylistManagerDLL() {
        playlist = new MyDoublyLinkedList<>();
        createAndShowGUI();
    }

    private void createAndShowGUI() {
        JFrame frame = new JFrame("Music Playlist Manager");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 300);

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(9, 1));

        JButton addButton = new JButton("Add a song");
        JButton removeButton = new JButton("Remove a song");
        JButton displayForwardButton = new JButton("Display playlist (Forward)");
        JButton displayBackwardButton = new JButton("Display playlist (Backward)");
        JButton searchButton = new JButton("Search for a song");
        JButton sizeButton = new JButton("Get playlist size");
        JButton exitButton = new JButton("Exit");

        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addSong();
            }
        });

        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeSong();
            }
        });

        displayForwardButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                displayPlaylistForward();
            }
        });

        displayBackwardButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                displayPlaylistBackward();
            }
        });

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                searchSong();
            }
        });

        sizeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getPlaylistSize();
            }
        });

        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        panel.add(addButton);
        panel.add(removeButton);
        panel.add(displayForwardButton);
        panel.add(displayBackwardButton);
        panel.add(searchButton);
        panel.add(sizeButton);
        panel.add(exitButton);

        frame.add(panel);
        frame.setVisible(true);
    }

    private void addSong() {
        String name = JOptionPane.showInputDialog("Enter the name of the song:");
        String genre = JOptionPane.showInputDialog("Enter the genre:");
        String artist = JOptionPane.showInputDialog("Enter the artist:");

        if (name != null && genre != null && artist != null) {
            Music newSong = new Music(name, genre, artist);
            playlist.add(newSong);
            JOptionPane.showMessageDialog(null, "Song added to the playlist.");
        }
    }

    private void removeSong() {
        String songToRemove = JOptionPane.showInputDialog("Enter the name of the song to remove:");

        if (songToRemove != null) {
            String songToRemoveLowerCase = songToRemove.toLowerCase(); // Convert input to lowercase

            // Iterate through the playlist to find a match with case-insensitive comparison
            boolean removed = false;
            DoublyLinkedNode<Music> current = playlist.getHead();
            while (current != null) {
                Music currentMusic = current.getData();
                String currentMusicNameLowerCase = currentMusic.getMusicName().toLowerCase(); // Convert playlist item to lowercase

                if (currentMusicNameLowerCase.equals(songToRemoveLowerCase)) {
                    // Found a case-insensitive match, remove the song
                    playlist.delete(currentMusic);
                    removed = true;
                    break; // Exit the loop
                }
                current = current.getNext();
            }

            if (removed) {
                JOptionPane.showMessageDialog(null, "Song removed from the playlist.");
            } else {
                JOptionPane.showMessageDialog(null, "Song not found in the playlist.");
            }
        }
    }

    private void displayPlaylistForward() {
        StringBuilder playlistText = new StringBuilder("Playlist (Forward):\n");
        DoublyLinkedNode<Music> current = playlist.getHead();
        while (current != null) {
            playlistText.append(current.getData()).append("\n");
            current = current.getNext();
        }
        JOptionPane.showMessageDialog(null, playlistText.toString());
    }

    private void displayPlaylistBackward() {
        StringBuilder playlistText = new StringBuilder("Playlist (Backward):\n");
        DoublyLinkedNode<Music> current = playlist.getTail();
        while (current != null) {
            playlistText.append(current.getData()).append("\n");
            current = current.getPrevious();
        }
        JOptionPane.showMessageDialog(null, playlistText.toString());
    }

    private void searchSong() {
        String songToSearch = JOptionPane.showInputDialog("Enter the name of the song to search for:");

        if (songToSearch != null) {
            songToSearch = songToSearch.toLowerCase(); // Convert the input to lowercase for case-insensitive search
            DoublyLinkedNode<Music> current = playlist.getHead();
            int index = 0;
            while (current != null) {
                String currentSongName = current.getData().getMusicName().toLowerCase(); // Convert song name to lowercase
                if (currentSongName.equals(songToSearch)) {
                    JOptionPane.showMessageDialog(null, "Song found at index " + index);
                    return; // Exit the method if found
                }
                current = current.getNext();
                index++;
            }
            JOptionPane.showMessageDialog(null, "Song not found in the playlist.");
        }
    }


    private void getPlaylistSize() {
        int size = playlist.getSize();
        JOptionPane.showMessageDialog(null, "Playlist size: " + size);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new MusicPlaylistManagerDLL();
            }
        });
    }
}
