/**
 * NAME: Modelo Alexx Evan O.
 * CLASS CODE: 9342 CS211
 * DATE: September 11, 2023
 */
package Prelim.Activities;

import java.util.ArrayList;
import java.util.Scanner;

public class MusicAppGrowArr {

    /**
     * Main method to run the music application.
     *
     * @param args The command-line arguments (not used in this program).
     */
    public static void main(String[] args) {
        // Create a MyList to store Music objects
        MyList<Music> musicList = new MyGrowingArrayList<>();
        // Create an ArrayList to store Music objects
        ArrayList<Music> musicArrayList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("This class uses the MyGrowingArrayList class. ");

        while (true) {
            System.out.println("--------------------------------");
            System.out.println("Options:");
            System.out.println("1. Insert a music record");
            System.out.println("2. Search for a music record");
            System.out.println("3. Delete a music record");
            System.out.println("4. Print the music list");
            System.out.println("5. Show current size of the list");
            System.out.println("6. Exit");
            System.out.println("--------------------------------");
            System.out.println("Enter your choice: ");

            int choice = scanner.nextInt();
            scanner.nextLine(); // Consume the newline character

            switch (choice) {
                case 1:
                    System.out.print("Enter music name: ");
                    String musicName = scanner.nextLine();
                    System.out.print("Enter genre: ");
                    String genre = scanner.nextLine();
                    System.out.print("Enter artist: ");
                    String artist = scanner.nextLine();

                    try {
                        Music music = new Music(musicName, genre, artist);
                        musicList.insert(music);
                        musicArrayList.add(music); // Add to the ArrayList as well
                        System.out.println("Music record inserted successfully.");
                    } catch (ListOverflowException e) {
                        System.out.println("ListOverflowException: " + e.getMessage());
                    }
                    break;

                case 2:
                    System.out.print("Enter music name to search: ");
                    String searchMusicName = scanner.nextLine();

                    boolean found = false;
                    for (Music m : musicArrayList) {
                        if (m.getMusicName().equalsIgnoreCase(searchMusicName)) {
                            System.out.println("Music record found:");
                            System.out.println(m);
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        System.out.println("Music record not found.");
                    }
                    break;

                case 3:
                    System.out.print("Enter music name to delete: ");
                    String deleteMusicName = scanner.nextLine();

                    boolean deleted = false;
                    for (Music m : musicArrayList) {
                        if (m.getMusicName().equalsIgnoreCase(deleteMusicName)) {
                            musicList.delete(m);
                            musicArrayList.remove(m);
                            System.out.println("Music record deleted.");
                            deleted = true;
                            break;
                        }
                    }

                    if (!deleted) {
                        System.out.println("Music record not found for deletion.");
                    }
                    break;

                case 4:
                    System.out.println("Music List:");
                    for (int i = 0; i < musicArrayList.size(); i++) {
                        Music music = musicArrayList.get(i);
                        System.out.println("Index " + i + ": " + music);
                    }
                    break;

                case 5:
                    int currentSize = musicList.getSize();
                    System.out.println("Current size of the list: " + currentSize);
                    break;

                case 6:
                    System.out.println("Exiting the program.");
                    scanner.close();
                    System.exit(0);
                    break;

                default:
                    System.out.println("Invalid choice. Please enter a valid option.");
            }
        }
    }
}
