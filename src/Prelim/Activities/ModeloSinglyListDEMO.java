package Prelim.Activities;

import java.util.NoSuchElementException;

public class ModeloSinglyListDEMO<E> implements MyList<E> {

    private LinkedListModeloAlexxEvanDEMO<E> head;

    private LinkedListModeloAlexxEvanDEMO<E> tail;


    public ModeloSinglyListDEMO(){
        head = null;
        tail = null;
    }//yesdas

    /**
     *
     * @return
     */
    @Override
    public int getSize() {
        return 0;
    }

    /**
     *
     * @param data
     * @throws ListOverflowException
     */
    @Override
    public void insert(E data) throws ListOverflowException {

    }

    /**
     *
     * @param data
     * @return
     * @throws NoSuchElementException
     */
    @Override
    public E getElement(E data) throws NoSuchElementException {
        return null;
    }

    /**
     *
     * @param data
     * @return
     */
    @Override
    public boolean delete(E data) {
        return false;
    }

    /**
     *
     * @param data
     * @return
     */
    @Override
    public int search(E data) {
        return 0;
    }
}
