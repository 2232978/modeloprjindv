/**
 * NAME: Modelo Alexx Evan O.
 * CLASS CODE: 9342 CS211
 * DATE: September 5, 2023
 *
 * Upload your source code for a class that may serve as a template of a Node of a Singly Linked List.  The code may be tentative since you may
 * need more time to get the essence of the exercise.  What is important is you, yourself, should do the exercise so that you will eventually
 * have a solid foundation of the LinkedList Data Structure..
 */

package Prelim.Activities;

import java.util.NoSuchElementException;
public interface MyList<E> {
    public int getSize();
    public void insert(E data) throws ListOverflowException;
    public E getElement(E data) throws NoSuchElementException;
    public boolean delete(E data); // returns false if the data is not deleted in the list
    public int search(E data); // returns index of data, else -1 is return
}