/**
 * NAME: Modelo Alexx Evan O.
 * CLASS CODE: 9342 CS211
 * DATE: September 5, 2023
 *
 * Upload your source code for a class that may serve as a template of a Node of a Singly Linked List.  The code may be tentative since you may
 * need more time to get the essence of the exercise.  What is important is you, yourself, should do the exercise so that you will eventually
 * have a solid foundation of the LinkedList Data Structure.
 */



package Prelim.Activities;

/**
 *
 * @param <T>
 */

public class LinkedListModeloAlexxEvanDEMO<T> {
    private T info;
    private LinkedListModeloAlexxEvanDEMO<T> link;


    public LinkedListModeloAlexxEvanDEMO(){
        this.info = null;
        this.link = null;
    }

    /**
     *
     * @param info
     */
    public LinkedListModeloAlexxEvanDEMO(T info){
        this.link = null;
        this.info = info;
    }
    /**
     *
     * @param datum
     * @param node
     */
    public LinkedListModeloAlexxEvanDEMO(T datum, LinkedListModeloAlexxEvanDEMO<T> node){
        info = datum;
        link = node;
    }

    /**
     *
     * @return
     */
    public T getInfo(){
        return info;
    }

    /**
     *
     * @return
     */
    public LinkedListModeloAlexxEvanDEMO<T> getLink(){
        return link;
    }

    /**
     *
     * @param info
     */
    public void setInfo(T info) {
        this.info = info;
    }

    /**
     *
     * @param link
     */
    public void setLink(LinkedListModeloAlexxEvanDEMO<T> link) {
        this.link = link;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "LinkedListNodeModelo{" +
                "info=" + info +
                ", link=" + link +
                '}';
    }
}
