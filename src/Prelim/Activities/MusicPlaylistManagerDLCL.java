package Prelim.Activities;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class MusicPlaylistManagerDLCL {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        MyDoublyLinkedCircularList<Music> musicList = new MyDoublyLinkedCircularList<>();
        int choice = 0;

        do {
            System.out.println("Music Playlist Menu:");
            System.out.println("1. Add Music");
            System.out.println("2. Search Music");
            System.out.println("3. Delete Music");
            System.out.println("4. Display Playlist");
            System.out.println("5. Exit");
            System.out.print("Enter your choice: ");

            try {
                choice = scanner.nextInt();
                scanner.nextLine(); // Consume newline character

                switch (choice) {
                    case 1:
                        System.out.print("Enter Music Name: ");
                        String musicName = scanner.nextLine();
                        System.out.print("Enter Genre: ");
                        String genre = scanner.nextLine();
                        System.out.print("Enter Artist: ");
                        String artist = scanner.nextLine();
                        Music newMusic = new Music(musicName, genre, artist);
                        musicList.insert(newMusic);
                        System.out.println("Music added successfully!");
                        break;
                    case 2:
                        System.out.print("Enter Music Name to search: ");
                        String searchName = scanner.nextLine();
                        int index = musicList.search(new Music(searchName, "", ""));
                        if (index != -1) {
                            System.out.println("Music found at index " + index);
                            // Retrieve and print the music at the found index
                            DoublyLinkedNode<Music> currentNode = musicList.getHead();
                            for (int i = 0; i < index; i++) {
                                currentNode = currentNode.getNext();
                            }
                            System.out.println(currentNode.getData());
                        } else {
                            System.out.println("Music not found in the playlist.");
                        }
                        break;
                    case 3:
                        System.out.print("Enter Music Name to delete: ");
                        String deleteName = scanner.nextLine();
                        boolean deleted = musicList.delete(new Music(deleteName, "", ""));
                        if (deleted) {
                            System.out.println("Music deleted successfully!");
                        } else {
                            System.out.println("Music not found in the playlist.");
                        }
                        break;
                    case 4:
                        System.out.println("Music Playlist:");
                        if (musicList.getHead() != null) {
                            DoublyLinkedNode<Music> currentNode = musicList.getHead();
                            do {
                                System.out.println(currentNode.getData());
                                currentNode = currentNode.getNext();
                            } while (currentNode != musicList.getHead());
                        } else {
                            System.out.println("Playlist is empty.");
                        }
                        break;
                    case 5:
                        System.out.println("Exiting Music Playlist.");
                        break;
                    default:
                        System.out.println("Invalid choice. Please try again.");
                        break;
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter a number.");
                scanner.nextLine(); // Consume invalid input
                choice = 0; // Reset choice to loop again
            } catch (NoSuchElementException e) {
                System.out.println("Element not found in the playlist.");
            }
        } while (choice != 5);

        scanner.close();
    }
}
