/**
 * NAME: Modelo Alexx Evan O.
 * CLASS CODE: 9342 CS211
 * DATE: September 11, 2023
 */

package Prelim.Activities;

import java.util.NoSuchElementException;

/**
 * A doubly linked list implementation that implements the MyList interface.
 *
 * @param <T> The type of elements stored in the list.
 */
public class MyDoublyLinkedList<T> implements MyList<T> {

    private DoublyLinkedNode<T> head;
    private DoublyLinkedNode<T> tail;
    private int size;

    /**
     * Constructs an empty doubly linked list.
     */
    public MyDoublyLinkedList() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    /**
     * Get the size of the doubly linked list.
     *
     * @return The number of elements in the list.
     */
    @Override
    public int getSize() {
        return size;
    }

    /**
     * Inserts an element at the end of the doubly linked list.
     *
     * @param data The data to be inserted.
     * @throws ListOverflowException If the list is full (not applicable to this implementation).
     */
    @Override
    public void insert(T data) throws ListOverflowException {
        add(data);
    }

    /**
     * Retrieves the first occurrence of an element in the doubly linked list.
     *
     * @param data The data to search for.
     * @return The first occurrence of the data in the list.
     * @throws NoSuchElementException If the element is not found in the list.
     */
    @Override
    public T getElement(T data) throws NoSuchElementException {
        DoublyLinkedNode<T> current = head;
        while (current != null) {
            if (current.getData().equals(data)) {
                return current.getData();
            }
            current = current.getNext();
        }
        throw new NoSuchElementException("Element not found in the list");
    }

    /**
     * Deletes the first occurrence of an element from the doubly linked list.
     *
     * @param data The data to be deleted.
     * @return True if the element was found and deleted; false otherwise.
     */
    @Override
    public boolean delete(T data) {
        DoublyLinkedNode<T> current = head;
        while (current != null) {
            if (current.getData().equals(data)) {
                if (current == head) {
                    head = current.getNext();
                    if (head != null) {
                        head.setPrevious(null);
                    }
                } else if (current == tail) {
                    tail = current.getPrevious();
                    if (tail != null) {
                        tail.setNext(null);
                    }
                } else {
                    DoublyLinkedNode<T> prev = current.getPrevious();
                    DoublyLinkedNode<T> next = current.getNext();
                    prev.setNext(next);
                    next.setPrevious(prev);
                }
                size--;
                return true;
            }
            current = current.getNext();
        }
        return false;
    }

    /**
     * Searches for the first occurrence of an element in the doubly linked list
     * with case-insensitive comparison.
     *
     * @param data The data to search for.
     * @return The index of the first occurrence of the data in the list, or -1 if not found.
     */
    public int search(T data) {
        DoublyLinkedNode<T> current = head;
        int index = 0;
        while (current != null) {
            if (current.getData() instanceof String &&
                    ((String) current.getData()).equalsIgnoreCase((String) data)) {
                return index;
            }
            current = current.getNext();
            index++;
        }
        return -1;
    }

    /**
     * Adds an element to the end of the doubly linked list.
     *
     * @param data The data to be added.
     */
    public void add(T data) {
        DoublyLinkedNode<T> newNode = new DoublyLinkedNode<>(data);
        if (head == null) {
            head = newNode;
            tail = newNode;
        } else {
            tail.setNext(newNode);
            newNode.setPrevious(tail);
            tail = newNode;
        }
        size++;
    }

    /**
     * Displays the elements of the doubly linked list in forward order.
     */
    public void displayForward() {
        DoublyLinkedNode<T> current = head;
        while (current != null) {
            System.out.print(current.getData() + " <-> ");
            current = current.getNext();
        }
        System.out.println("null");
    }

    /**
     * Displays the elements of the doubly linked list in reverse order.
     */
    public void displayBackward() {
        DoublyLinkedNode<T> current = tail;
        while (current != null) {
            System.out.print(current.getData() + " <-> ");
            current = current.getPrevious();
        }
        System.out.println("null");
    }

    /**
     * Get the head node of the doubly linked list.
     *
     * @return The head node of the list.
     */
    public DoublyLinkedNode<T> getHead() {
        return head;
    }

    /**
     * Get the tail node of the doubly linked list.
     *
     * @return The tail node of the list.
     */
    public DoublyLinkedNode<T> getTail() {
        return tail;
    }
}
