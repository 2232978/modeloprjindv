/**
 * NAME: Modelo Alexx Evan O.
 * CLASS CODE: 9342 CS211
 * DATE: September 5, 2023
 *
 */

package Prelim.Activities;

public class ListOverflowException extends RuntimeException {
    public ListOverflowException(String s){
        super("List has reached maximum capacity.");
    }
}
