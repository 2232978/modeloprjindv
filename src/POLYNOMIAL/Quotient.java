package POLYNOMIAL;

public class Quotient {
    private Polynomial quotient;
    private Polynomial remainder;

    // Constructor
    public Quotient(Polynomial quotient, Polynomial remainder) {
        this.quotient = quotient;
        this.remainder = remainder;
    }

    // Getter method for quotient
    public Polynomial getQuotient() {
        return quotient;
    }

    // Getter method for remainder
    public Polynomial getRemainder() {
        return remainder;
    }

    // Convert the quotient object to a string representation
    @Override
    public String toString() {
        return "Quotient: " + quotient.toString() + "\nRemainder: " + remainder.toString();
    }
}

