package POLYNOMIAL;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;

public class PolynomialArithmetic {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        LinkedList<Polynomial> polynomials = new LinkedList<>();

        while (true) {
            System.out.println("Polynomial Arithmetic Menu:");
            System.out.println("1. Create Polynomial");
            System.out.println("2. Perform Addition");
            System.out.println("3. Perform Subtraction");
            System.out.println("4. Perform Multiplication");
            System.out.println("5. Perform Division");
            System.out.println("6. Evaluate Polynomial");
            System.out.println("7. Exit");
            System.out.print("Enter your choice: ");

            try {
                int choice = scanner.nextInt();
                scanner.nextLine(); // Consume the newline character

                switch (choice) {
                    case 1:
                        Polynomial polynomial = createPolynomial(scanner);
                        polynomials.add(polynomial);
                        System.out.println("Polynomial created: " + polynomial.toString());
                        break;
                    case 2:
                        performArithmeticOperation(polynomials, "addition");
                        break;
                    case 3:
                        performArithmeticOperation(polynomials, "subtraction");
                        break;
                    case 4:
                        performArithmeticOperation(polynomials, "multiplication");
                        break;
                    case 5:
                        performArithmeticOperation(polynomials, "division");
                        break;
                    case 6:
                        evaluatePolynomial(polynomials, scanner);
                        break;
                    case 7:
                        System.out.println("Exiting the program.");
                        scanner.close();
                        System.exit(0);
                    default:
                        System.out.println("Invalid choice. Please enter a valid option.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter a valid number.");
                scanner.nextLine(); // Consume the invalid input
            }
        }
    }

    private static Polynomial createPolynomial(Scanner scanner) {
        Polynomial polynomial = new Polynomial();
        while (true) {
            try {
                System.out.print("Enter numerical coefficient (constant term or 0 for none): ");
                int numericalCoefficient = scanner.nextInt();
                scanner.nextLine(); // Consume the newline character

                if (numericalCoefficient == 0) {
                    break; // Stop if the user enters 0 for the numerical coefficient
                }

                System.out.print("Enter literal coefficient (a single character or press Enter for none): ");
                String literalCoefficientInput = scanner.nextLine();
                char literalCoefficient = (literalCoefficientInput.isEmpty()) ? '\0' : literalCoefficientInput.charAt(0);

                System.out.print("Enter exponent (0 for a constant term): ");
                int exponent = scanner.nextInt();
                scanner.nextLine(); // Consume the newline character

                Term term = new Term(numericalCoefficient, literalCoefficient, exponent);
                polynomial.addTerm(term);

                System.out.print("Do you want to add another term to this polynomial? (y/n): ");
                String choice = scanner.nextLine().toLowerCase();
                if (!choice.equals("y")) {
                    break;
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter valid coefficients and exponent.");
                scanner.nextLine(); // Consume the invalid input
            } catch (StringIndexOutOfBoundsException e) {
                System.out.println("Literal coefficient must be a single character.");
            }
        }
        return polynomial;
    }

    private static void performArithmeticOperation(LinkedList<Polynomial> polynomials, String operation) {
        if (polynomials.size() < 2) {
            System.out.println("You need at least two polynomials to perform " + operation + ".");
            return;
        }

        System.out.println("Available Polynomials:");
        for (int i = 0; i < polynomials.size(); i++) {
            System.out.println(i + ". " + polynomials.get(i).toString());
        }

        System.out.print("Enter the indices of the two polynomials to perform " + operation + " (e.g., 0 1): ");
        Scanner scanner = new Scanner(System.in);
        try {
            int index1 = scanner.nextInt();
            int index2 = scanner.nextInt();

            if (index1 < 0 || index1 >= polynomials.size() || index2 < 0 || index2 >= polynomials.size()) {
                System.out.println("Invalid polynomial indices. Please enter valid indices.");
                return;
            }

            Polynomial result = null;
            if (operation.equals("addition")) {
                result = polynomials.get(index1).add(polynomials.get(index2));
            } else if (operation.equals("subtraction")) {
                result = polynomials.get(index1).subtract(polynomials.get(index2));
            } else if (operation.equals("multiplication")) {
                result = polynomials.get(index1).multiply(polynomials.get(index2));
            } else if (operation.equals("division")) {
                Quotient quotientResult = polynomials.get(index1).divide(polynomials.get(index2));
                if (quotientResult != null) {
                    System.out.println("Division Result:");
                    System.out.println(quotientResult.toString());
                } else {
                    System.out.println("Division by zero or other error occurred.");
                }
                return;
            }

            if (result != null) {
                polynomials.add(result);
                System.out.println(operation + " result: " + result.toString());
            }
        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Please enter valid indices.");
            scanner.nextLine(); // Consume the invalid input
        }
    }

    private static void evaluatePolynomial(LinkedList<Polynomial> polynomials, Scanner scanner) {
        if (polynomials.isEmpty()) {
            System.out.println("No polynomials available for evaluation.");
            return;
        }

        System.out.println("Available Polynomials:");
        for (int i = 0; i < polynomials.size(); i++) {
            System.out.println(i + ". " + polynomials.get(i).toString());
        }

        System.out.print("Enter the index of the polynomial to evaluate: ");
        try {
            int index = scanner.nextInt();

            if (index < 0 || index >= polynomials.size()) {
                System.out.println("Invalid polynomial index. Please enter a valid index.");
                return;
            }

            System.out.print("Enter the value of x for evaluation: ");
            double xValue = scanner.nextDouble();
            double evaluationResult = polynomials.get(index).evaluate(xValue);
            System.out.println("Evaluation result: " + evaluationResult);
        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Please enter a valid index and value.");
            scanner.nextLine(); // Consume the invalid input
        }
    }
}

