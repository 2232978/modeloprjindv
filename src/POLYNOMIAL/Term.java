package POLYNOMIAL;

public class Term {
    private int numericalCoefficient;
    private char literalCoefficient;
    private int exponent;

    // Default constructor
    public Term() {
        this(0, 'x', 0); // Default values
    }

    // Parameterized constructor
    public Term(int numericalCoefficient, char literalCoefficient, int exponent) {
        this.numericalCoefficient = numericalCoefficient;
        this.literalCoefficient = literalCoefficient;
        this.exponent = exponent;
    }

    // Getter methods
    public int getNumericalCoefficient() {
        return numericalCoefficient;
    }

    public char getLiteralCoefficient() {
        return literalCoefficient;
    }

    public int getExponent() {
        return exponent;
    }

    // Setter methods
    public void setNumericalCoefficient(int numericalCoefficient) {
        this.numericalCoefficient = numericalCoefficient;
    }

    public void setLiteralCoefficient(char literalCoefficient) {
        this.literalCoefficient = literalCoefficient;
    }

    public void setExponent(int exponent) {
        this.exponent = exponent;
    }

    // Compare two terms based on their exponents
    public int compareTo(Term otherTerm) {
        return Integer.compare(this.exponent, otherTerm.exponent);
    }

    // Check if two terms are equal
    public boolean equals(Term anotherTerm) {
        return this.numericalCoefficient == anotherTerm.numericalCoefficient
                && this.literalCoefficient == anotherTerm.literalCoefficient
                && this.exponent == anotherTerm.exponent;
    }

    // Convert term to a string representation
    @Override
    public String toString() {
        return numericalCoefficient + String.valueOf(literalCoefficient) + "^" + exponent;
    }
}

