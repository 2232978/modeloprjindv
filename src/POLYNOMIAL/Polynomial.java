package POLYNOMIAL;

import java.util.LinkedList;

public class Polynomial {
    private LinkedList<Term> terms;

    // Default constructor
    public Polynomial() {
        this.terms = new LinkedList<>();
    }

    // Getter method for terms
    public LinkedList<Term> getTerms() {
        return terms;
    }

    // Setter method for terms
    public void setTerms(LinkedList<Term> terms) {
        this.terms = terms;
    }

    // Add a new term to the polynomial
    public void addTerm(Term newTerm) {
        terms.add(newTerm);
    }

    // Convert the polynomial to a string representation
    @Override
    public String toString() {
        if (terms.isEmpty()) {
            return "0"; // If there are no terms, the polynomial is zero
        }

        StringBuilder polynomialString = new StringBuilder();
        for (Term term : terms) {
            if (term.getNumericalCoefficient() != 0) {
                if (polynomialString.length() > 0) {
                    polynomialString.append(" + ");
                }
                polynomialString.append(term.toString());
            }
        }
        return polynomialString.toString();
    }

    // Evaluate the polynomial for a given value
    public double evaluate(double value) {
        double result = 0.0;
        for (Term term : terms) {
            result += term.getNumericalCoefficient() * Math.pow(value, term.getExponent());
        }
        return result;
    }

    // Add two polynomials
// Add two polynomials
    public Polynomial add(Polynomial another) {
        Polynomial result = new Polynomial();

        LinkedList<Term> resultTerms = new LinkedList<>(this.terms); // Start with a copy of the terms in this polynomial

        for (Term term : another.getTerms()) {
            boolean added = false; // Flag to check if the term was added
            for (Term existingTerm : resultTerms) {
                if (term.getExponent() == existingTerm.getExponent() && term.getLiteralCoefficient() == existingTerm.getLiteralCoefficient()) {
                    // If the exponents and literal coefficients match, add the numerical coefficients
                    existingTerm.setNumericalCoefficient(existingTerm.getNumericalCoefficient() + term.getNumericalCoefficient());
                    added = true;
                    break;
                }
            }
            if (!added) {
                // If the term was not added, add it as a new term
                resultTerms.add(term);
            }
        }

        result.setTerms(resultTerms);
        return result;
    }


    // Subtract two polynomials
    public Polynomial subtract(Polynomial another) {
        Polynomial result = new Polynomial();
        result.getTerms().addAll(this.terms);
        for (Term term : another.getTerms()) {
            Term negatedTerm = new Term(-term.getNumericalCoefficient(), term.getLiteralCoefficient(), term.getExponent());
            result.getTerms().add(negatedTerm);
        }
        return result;
    }

    // Multiply two polynomials
    public Polynomial multiply(Polynomial another) {
        Polynomial result = new Polynomial();
        for (Term term1 : this.terms) {
            for (Term term2 : another.getTerms()) {
                int newNumericalCoefficient = term1.getNumericalCoefficient() * term2.getNumericalCoefficient();
                char newLiteralCoefficient = term1.getLiteralCoefficient(); // Assuming literal coefficients are the same
                int newExponent = term1.getExponent() + term2.getExponent();
                Term newTerm = new Term(newNumericalCoefficient, newLiteralCoefficient, newExponent);
                result.addTerm(newTerm);
            }
        }
        return result;
    }

    // Divide two polynomials and return the quotient
    public Quotient divide(Polynomial another) {
        // Initialize the quotient and remainder
        Polynomial quotient = new Polynomial();
        Polynomial remainder = new Polynomial();
        remainder.setTerms(new LinkedList<>(this.getTerms())); // Initialize remainder with the dividend

        // Perform long division
        while (!remainder.getTerms().isEmpty() && remainder.getTerms().getLast().getExponent() >= another.getTerms().getLast().getExponent()) {
            Term leadingTermRemainder = remainder.getTerms().getLast();
            Term leadingTermAnother = another.getTerms().getLast();

            int numericalCoefficient = leadingTermRemainder.getNumericalCoefficient() / leadingTermAnother.getNumericalCoefficient();
            char literalCoefficient = leadingTermRemainder.getLiteralCoefficient();
            int exponent = leadingTermRemainder.getExponent() - leadingTermAnother.getExponent();

            Term quotientTerm = new Term(numericalCoefficient, literalCoefficient, exponent);
            Polynomial temp = new Polynomial();
            temp.addTerm(quotientTerm);
            quotient = quotient.add(temp);

            Polynomial product = another.multiply(temp);
            remainder = remainder.subtract(product);
        }

        Quotient result = new Quotient(quotient, remainder);
        return result;
    }

}

