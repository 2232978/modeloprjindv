package Project2Tries;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class GmailSimulation {
    public static void main(String[] args) {
        // Create MainList with inner lists (Inbox, Sent, Trash)
        List<List<Email>> mainList = new LinkedList<>();
        EmailManager emailManager = new EmailManager(mainList);
        mainList.add(new LinkedList<>()); // Inbox
        mainList.add(new LinkedList<>()); // Sent
        mainList.add(new LinkedList<>()); // Trash

        Scanner scanner = new Scanner(System.in);
        int choice;

        do {
            System.out.println("Gmail Simulation Menu:");
            System.out.println("1. Compose and Send Email");
            System.out.println("2. View Inbox");
            System.out.println("3. View Sent Items");
            System.out.println("4. View Trash");
            System.out.println("5. Move Email to Trash");
            System.out.println("6. Search Email by Subject");
            System.out.println("7. Search Email by Sender");
            System.out.println("8. Search Email by Recipient");
            System.out.println("9. Search Email by Date");
            System.out.println("10. Restore Email from Trash");
            System.out.println("11. Exit");
            System.out.print("Enter your choice: ");

            choice = scanner.nextInt();
            scanner.nextLine(); // Consume newline character

            switch (choice) {
                case 1:
                    // Compose and send email (add to Sent)
                    composeAndSendEmail(emailManager, mainList);
                    break;
                case 2:
                    // View Inbox
                    emailManager.viewEmails(mainList.get(0), "Inbox");
                    break;
                case 3:
                    // View Sent Items
                    emailManager.viewEmails(mainList.get(1), "Sent Items");
                    break;
                case 4:
                    // View Trash
                    emailManager.viewEmails(mainList.get(2), "Trash");
                    break;
                case 5:
                    // Move email to Trash
                    System.out.print("Enter the source list index (0 for Inbox, 1 for Sent): ");
                    int sourceListIndex = scanner.nextInt();
                    System.out.print("Enter the email index to move to Trash: ");
                    int emailIndex = scanner.nextInt();
                    emailManager.moveEmailToTrash(sourceListIndex, emailIndex);
                    break;

                case 6:
                    // Search email by subject
                    searchByEmailSubject(emailManager);
                    break;

                case 7:
                    // Search email by sender
                    System.out.print("Enter sender to search: ");
                    String searchSender = scanner.nextLine();
                    emailManager.searchByEmailSender(searchSender);
                    break;
                case 8:
                    // Search email by recipient
                    searchByEmailRecipient(emailManager);
                    break;
                case 9:
                    // Search email by date
                    searchByEmailDate(emailManager);
                    break;
                case 10:
                    // Restore email from Trash
                    System.out.print("Enter the email index to restore from Trash: ");
                    emailIndex = scanner.nextInt();
                    System.out.print("Enter the target list index (0 for Inbox, 1 for Sent): ");
                    int targetListIndex = scanner.nextInt();
                    emailManager.restoreEmail(emailIndex, targetListIndex);
                    break;

                case 11:
                    System.out.println("Exiting Gmail Simulation. Goodbye!");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
                    break;
            }
        } while (choice != 10);

        scanner.close();
    }

    // Method to compose and send email
    static void composeAndSendEmail(EmailManager emailManager, List<List<Email>> mainList) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter recipient: ");
        String recipient = scanner.nextLine();
        System.out.print("Enter subject: ");
        String subject = scanner.nextLine();
        System.out.print("Enter email content: ");
        String content = scanner.nextLine();
        emailManager.viewEmails(mainList.get(1), "Sent Items");
        // Compose and send email (add to Sent)
        emailManager.composeAndSendEmail(recipient, subject, content);

        System.out.println("Email sent!");
    }

    // Method to search email by subject
    static void searchByEmailSubject(EmailManager emailManager) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter subject to search: ");
        String searchSubject = scanner.nextLine();

        emailManager.searchByEmailSubject(searchSubject);
    }

    // Method to search email by recipient
    static void searchByEmailRecipient(EmailManager emailManager) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter recipient to search: ");
        String searchRecipient = scanner.nextLine();

        emailManager.searchByEmailRecipient(searchRecipient);
    }

    // Method to search email by date
    static void searchByEmailDate(EmailManager emailManager) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter date to search (YYYY-MM-DD): ");
        String searchDateStr = scanner.nextLine();

        emailManager.searchByEmailDate(searchDateStr);
    }
}
