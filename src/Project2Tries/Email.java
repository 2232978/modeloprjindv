package Project2Tries;

import java.util.Date;
import java.util.Objects;

public class Email {
    private String sender;
    private String recipient;
    private String subject;
    private String content;
    Date date;
    boolean isDeleted;


    public Email(String sender, String recipient,String content, String subject, Date date){
        this.recipient = recipient;
        this.subject = subject;
        this.content = content;
        this.sender = sender;
        this.date = date;
        this.isDeleted = false;

    }

    public String getRecipient() {
        return recipient;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    public String getSender() {
        return sender;
    }

    public Date getDate() {
        return date;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setTimestamp(Date timestamp) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Email email = (Email) o;
        return Objects.equals(recipient, email.recipient) && Objects.equals(subject, email.subject) && Objects.equals(content, email.content) && Objects.equals(sender, email.sender) && Objects.equals(date, email.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipient, subject, content, sender, date);
    }

    @Override
    public String toString() {
        return "Email{" +
                "recipient='" + recipient + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", sender='" + sender + '\'' +
                ", timestamp='" + date + '\'' +
                '}';
    }
}

