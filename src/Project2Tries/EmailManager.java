package Project2Tries;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.text.SimpleDateFormat;
import java.text.ParseException;

public class EmailManager {
    private final List<List<Email>> mainList;

    public EmailManager(List<List<Email>> mainList) {
        this.mainList = mainList;
        // Initialize MainList with inner lists (Inbox, Sent, Trash)
        mainList.add(new LinkedList<>()); // Inbox
        mainList.add(new LinkedList<>()); // Sent
        mainList.add(new LinkedList<>()); // Trash
        addSampleEmails();
    }

    public void composeAndSendEmail(String recipient, String subject, String content) {
        // Create and send an email (add to Sent)
        Email email = new Email("me", recipient, subject, content, new Date());
        mainList.get(1).add(email); // Adding to "Sent" folder
    }


    public void viewEmails(List<Email> emailList, String listName) {
        // View emails in a list (Inbox, Sent Items, Trash)
        if (emailList.isEmpty()) {
            System.out.println(listName + " is empty.");
            return;
        }

        System.out.println("=== " + listName + " ===");
        int emailCount = 0;

        for (Email email : emailList) {
            if (!email.isDeleted) {
                printEmail(email);
                emailCount++;
            }
        }

        if (emailCount == 0) {
            System.out.println(listName + " is empty.");
        }
    }

    public void moveEmailToTrash(int sourceListIndex, int emailIndex) {
        // Move an email to Trash
        if (sourceListIndex >= 0 && sourceListIndex < mainList.size()) {
            List<Email> sourceList = mainList.get(sourceListIndex);
            if (emailIndex >= 0 && emailIndex < sourceList.size()) {
                Email emailToMove = sourceList.get(emailIndex);
                emailToMove.isDeleted = true;
                mainList.get(2).add(emailToMove); // Move to Trash
                System.out.println("Email moved to Trash!");
            } else {
                System.out.println("Invalid email index.");
            }
        } else {
            System.out.println("Invalid source list index.");
        }
    }


    public void searchByEmailSubject(String searchSubject) {
        // Search for emails by subject
        System.out.println("Search Results:");
        int emailCount = 0;

        for (Email email : mainList.get(0)) { // Search in Inbox
            if (!email.isDeleted && email.getSubject().toLowerCase().contains(searchSubject.toLowerCase())) {
                printEmail(email);
                emailCount++;
            }
        }

        for (Email email : mainList.get(1)) { // Search in Sent Items
            if (!email.isDeleted && email.getSubject().toLowerCase().contains(searchSubject.toLowerCase())) {
                printEmail(email);
                emailCount++;
            }
        }

        if (emailCount == 0) {
            System.out.println("No matching emails found.");
        }
    }

    public void searchByEmailRecipient(String searchRecipient) {
        // Search for emails by recipient
        System.out.println("Search Results:");
        int emailCount = 0;

        for (Email email : mainList.get(0)) { // Search in Inbox
            if (!email.isDeleted && email.getRecipient().toLowerCase().contains(searchRecipient.toLowerCase())) {
                printEmail(email);
                emailCount++;
            }
        }

        for (Email email : mainList.get(1)) { // Search in Sent Items
            if (!email.isDeleted && email.getRecipient().toLowerCase().contains(searchRecipient.toLowerCase())) {
                printEmail(email);
                emailCount++;
            }
        }

        if (emailCount == 0) {
            System.out.println("No matching emails found.");
        }
    }

    public void searchByEmailDate(String searchDateStr) {
        // Search for emails by date (format: "yyyy-MM-dd")
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date searchDate;

        try {
            searchDate = dateFormat.parse(searchDateStr);
        } catch (ParseException e) {
            System.out.println("Invalid date format. Use YYYY-MM-DD.");
            return;
        }

        System.out.println("Search Results:");
        int emailCount = 0;

        for (Email email : mainList.get(0)) { // Search in Inbox
            if (!email.isDeleted && email.getDate().equals(searchDate)) {
                printEmail(email);
                emailCount++;
            }
        }

        for (Email email : mainList.get(1)) { // Search in Sent Items
            if (!email.isDeleted && email.getDate().equals(searchDate)) {
                printEmail(email);
                emailCount++;
            }
        }

        if (emailCount == 0) {
            System.out.println("No matching emails found.");
        }
    }

    public void searchByEmailSender(String searchSender) {
        // Search for emails by sender
        System.out.println("Search Results:");
        int emailCount = 0;

        for (Email email : mainList.get(0)) { // Search in Inbox
            if (!email.isDeleted && email.getSender().toLowerCase().contains(searchSender.toLowerCase())) {
                printEmail(email);
                emailCount++;
            }
        }

        for (Email email : mainList.get(1)) { // Search in Sent Items
            if (!email.isDeleted && email.getSender().toLowerCase().contains(searchSender.toLowerCase())) {
                printEmail(email);
                emailCount++;
            }
        }

        if (emailCount == 0) {
            System.out.println("No matching emails found.");
        }
    }


    public void restoreEmail(int emailIndex, int targetListIndex) {
        // Restore an email from Trash to Inbox or Sent Items
        if (emailIndex >= 0 && emailIndex < mainList.get(2).size() &&
                targetListIndex >= 0 && targetListIndex < mainList.size()) {

            Email emailToRestore = mainList.get(2).get(emailIndex);
            emailToRestore.isDeleted = false;
            mainList.get(targetListIndex).add(emailToRestore);
            System.out.println("Email restored!");
        } else {
            System.out.println("Invalid email index or target list index.");
        }
    }



    private void printEmail(Email email) {
        System.out.println("Sender: " + email.getSender());
        System.out.println("Recipient: " + email.getRecipient());
        System.out.println("Subject: " + email.getSubject());
        System.out.println("Content: " + email.getContent());
        System.out.println("Date: " + email.getDate());
        System.out.println("Status: " + (email.isDeleted ? "Deleted" : "Not Deleted"));
        System.out.println();
    }

    private void addSampleEmails() {
        // Add sample emails to the Inbox
        mainList.get(0).add(new Email("sender1", "me", "Sample Email 1", "Hello!", new Date()));
        mainList.get(0).add(new Email("sender2", "me", "Sample Email 2", "Hi there!", new Date()));

        // Add sample emails to the Trash
        mainList.get(2).add(new Email("sender3", "me", "Sample Email 3", "This is deleted.", new Date()));
    }
}
